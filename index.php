<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <title>testar</title>
        <script>
            function verInsertar(){
                var contenido = "<table><tr>"+
                        "<td>Id: </td><td><input type='text' id='t1'/></td>"+
                        "</tr><tr>"+
                        "<td>Descripcion: </td><td><input type='text' id='t2'/></td>"+
                        "</tr><tr>"+
                        "<td colspan='2'><input type='button' value='OK' onclick='javascript: irInsertar();'/></td>"+
                        "</tr></table>";
                
                document.getElementById("contenido").innerHTML = contenido;
                document.getElementById("pie").innerHTML = "";
            }
            function verActualizar(){
                var contenido = "<table><tr>"+
                        "<td>Id: </td><td><input type='text' id='t1'/></td>"+
                        "</tr><tr>"+
                        "<td>Descripcion: </td><td><input type='text' id='t2'/></td>"+
                        "</tr><tr>"+
                        "<td><input type='button' value='BUSCAR' onclick='javascript: irBuscar();'/></td>"+
                                    "<td><input type='button' value='OK' onclick='javascript: irActualizar();'/></td>"+
                        "</tr></table>";
                
                document.getElementById("contenido").innerHTML = contenido;
                document.getElementById("pie").innerHTML = "";
            }
            function verEliminar(){
                var contenido = "<table><tr>"+
                        "<td>Id: </td><td><input type='text' id='t1'/></td>"+
                        "</tr><tr>"+            
                        "<td>Descripcion: </td><td><input type='text' id='t2'/></td>"+
                        "</tr><tr>"+
                        "<td><input type='button' value='BUSCAR' onclick='javascript: irBuscarEliminar();'/></td>"+
                                    "<td><input type='button' value='ELIMINAR' onclick='javascript: irEliminar();'/></td>"+
                        "</tr></table>";
                
                document.getElementById("contenido").innerHTML = contenido;
                document.getElementById("pie").innerHTML = "";
            }
            function verConsultar(){
                var contenido = "<table><tr>"+
                        "<td><input type='button' value='BUSCAR' onclick='javascript: irBuscarTodos();'/></td></td>"+
                        "</tr></table>";
                
                document.getElementById("contenido").innerHTML = contenido;
                
            }
            function irInsertar(){
                var t1 = document.getElementById("t1").value;
                var t2 = document.getElementById("t2").value;
                
                if (t2 == "" || t1 == ""){
                    document.getElementById("pie").innerHTML = "Debe llenar todos los datos.";
                }else{
                
                    $.ajax({
                        url:    'exe_ingreso.php',
                        type:   'GET',
                        data:   't1='          + t1 +
                                '&t2='         + t2 ,
                        beforeSend: function(){
                            var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                            var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                            document.getElementById("pie").innerHTML = contenido;
                        },
                        success: function () {
                            document.getElementById("pie").innerHTML = "Ingresado con exito";
                            document.getElementById("t1").value = "";
                            document.getElementById("t2").value = "";
                        },
                        error: function (e) {
                            document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                        }
                    });
                }
            }
            function irBuscar(){
                var t1 = document.getElementById("t1").value;
                
                $.ajax({
                    url:    'exe_buscar.php',
                    type:   'GET',
                    data:   't1='          + t1 ,
                    beforeSend: function(){
                        var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                        var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                        document.getElementById("pie").innerHTML = contenido;
                    },
                    success: function (data) {
                        document.getElementById("contenido").innerHTML = data;
                        document.getElementById("pie").innerHTML = "";
                        
                        if (data === ""){
                            verActualizar();
                            document.getElementById("pie").innerHTML = "No se encontro el dato";
                        }
                    },
                    error: function (e) {
                        document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                    }
                });
            }
            function irActualizar(){
                var t1 = document.getElementById("t1").value;
                var t2 = document.getElementById("t2").value;
                
                if (t2 == "" || t1 == ""){
                    document.getElementById("pie").innerHTML = "Primero debe buscar el registro.";
                }else{
                    $.ajax({
                        url:    'exe_actualizar.php',
                        type:   'GET',
                        data:   't1='          + t1 +
                                '&t2='         + t2 ,
                        beforeSend: function(){
                            var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                            var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                            document.getElementById("pie").innerHTML = contenido;
                        },
                        success: function () {
                            document.getElementById("pie").innerHTML = "Actualizado con exito";
                            document.getElementById("t1").value = "";
                            document.getElementById("t2").value = "";
                        },
                        error: function (e) {
                            document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                        }
                    });
                }
            }
            function irBuscarEliminar(){
                var t1 = document.getElementById("t1").value;
                
                $.ajax({
                    url:    'exe_buscarEliminar.php',
                    type:   'GET',
                    data:   't1='          + t1 ,
                    beforeSend: function(){
                        var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                        var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                        document.getElementById("pie").innerHTML = contenido;
                    },
                    success: function (data) {
                        document.getElementById("contenido").innerHTML = data;
                        document.getElementById("pie").innerHTML = "";
                        
                        if (data === ""){
                            verEliminar();
                            document.getElementById("pie").innerHTML = "No se encontro el dato";
                        }
                    },
                    error: function (e) {
                        document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                    }
                });
            }
            function irEliminar(){
                var t1 = document.getElementById("t1").value;
                var t2 = document.getElementById("t2").value;
                
                if (t2 == "" || t1 == ""){
                    document.getElementById("pie").innerHTML = "Primero debe buscar el registro.";
                }else{
                    $.ajax({
                        url:    'exe_eliminar.php',
                        type:   'GET',
                        data:   't1='          + t1 +
                                '&t2='         + t2 ,
                        beforeSend: function(){
                            var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                            var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                            document.getElementById("pie").innerHTML = contenido;
                        },
                        success: function () {
                            document.getElementById("pie").innerHTML = "Eliminado con exito";
                            document.getElementById("t1").value = "";
                            document.getElementById("t2").value = "";
                        },
                        error: function (e) {
                            document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                        }
                    });
                }
            }
            function irBuscarTodos(){
                
                $.ajax({
                    url:    'exe_buscarTodos.php',
                    type:   'GET',
                    data:   '',
                    beforeSend: function(){
                        var imagen      = "<img src='1.gif' width='15px' height='15px' />";
                        var contenido   = "<div style='text-align: left; padding: 10px 0px'>"+imagen+"</div>";
                        document.getElementById("pie").innerHTML = contenido;
                    },
                    success: function (data) {
                        document.getElementById("contenido").innerHTML = data;
                        document.getElementById("pie").innerHTML = "";
                        
                        if (data === ""){
                            verActualizar();
                            document.getElementById("pie").innerHTML = "No se encontro datos";
                        }
                    },
                    error: function (e) {
                        document.getElementById("pie").innerHTML = "Ocurrio un error. Vuelve a intentarlo mas tarde.";
                    }
                });
            }
        </script>
    </head>
    <body>
        <div id="cabecera">
            <a href="javascript: verInsertar();">Insertar</a>
            <a href="javascript: verActualizar();">Actualizar</a>
            <a href="javascript: verEliminar();">Eliminar</a>
            <a href="javascript: verConsultar();">Consultar</a>
        </div>
        <div id="contenido"></div>
        <div id="pie"></div>
    </body>
</html>

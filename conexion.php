<?php

require_once 'lib/php-activerecord/ActiveRecord.php';
ActiveRecord\Config::initialize(function($cfg) {
    $bd = 'test';
    $user = 'root';
    $pass = 'root123';
    $ip = '127.0.0.1';
    $cfg->set_model_directory('bean');
    $cfg->set_connections(array(
        'development' => 'mysql://'.$user.':'.$pass.'@'.$ip.'/'.$bd.''));
});


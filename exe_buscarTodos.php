<?php

include("conexion.php");

$result         = Prueba::all();

$contenido      = "<table border='1' cellpadding='0' cellspacing='0'><tr><th width='100px'>Id</th><th width='200px'>Descripcion</th><th>Fecha Manual</th></tr>";
$detalle        = "";
$paso           = "NOK";
foreach ($result as $registro){
$paso           = "OK";

$fecha_hoy      = new DateTime();
$fecha_hoy      = $registro->fecha_registro;
$fecha_hoy      = $fecha_hoy->format('Y-m-d H:i:s');

$fecha_manual   = new DateTime();
$fecha_manual   = $registro->fecha_manual;
$fecha_manual   = $fecha_manual->format('Y-m-d');

$detalle        = $detalle . "<tr>".
                                "<td style='text-align: center'>".$registro->id."</td>".
                                "<td style='text-align: center'>".$registro->descripcion."</td>".
                                "<td style='text-align: center'>".$fecha_manual." y ".$fecha_hoy."</td>".
                                "</tr>";
}

if ($paso == "NOK"){
$detalle        = "<tr>".
                    "<td style='text-align: center' colspan='2'>No se encontro registros</td>".
                    "</tr>";
}

$contenido = $contenido . $detalle .  "</table>";

echo $contenido;